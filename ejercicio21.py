import sys
import getopt
from multiprocessing import Pool


def root(n):
    return n**(1/2)


(opt, arg) = getopt.getopt(sys.argv[1:], 'n:m:')

n = 0
n2 = 0

for (op, ar) in opt:

    if op == '-n':
        n = int(ar)
    elif op == '-m':
        n2 = int(ar)

pool = Pool()
n_impar = []

for x in range(n, n2):
    if x % 2 != 0:
        n_impar.append(x)

results = pool.map(root, n_impar)
for x, y in zip(n_impar, results):
    print("La raíz cuadrada de %d: %f" % (x, y))
