import os
import sys


def childProcess1(p):
    os.close(r)
    p = os.fdopen(p, 'w')
    print("Escribe un mensaje:\n")
    for line in sys.stdin:
        p.write("%s" % line)
        p.flush()


def childProcess2(q):
    os.close(w)
    q = os.fdopen(q)
    while True:
        line = q.readline()
        if line:
            print("Leyendo (PID=%d): %s" % (os.getpid(), line))
        else:
            sys.exit(0)


r, w = os.pipe()
pid = os.fork()
if pid == 0:
    childProcess1(w)
    os._exit(0)
else:
    pid2 = os.fork()
    if pid2 == 0:
        os.close(w)
        r = os.fdopen(r)
        while True:
            line = r.readline()
            if line:
                print("Leyendo (PID=%d): %s" % (os.getpid(), line))
            else:
                sys.exit(0)
        os._exit(0)
    os.wait()
