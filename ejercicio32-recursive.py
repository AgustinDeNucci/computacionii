from concurrent.futures import ProcessPoolExecutor
import sys
import getopt

(opt, arg) = getopt.getopt(sys.argv[1:], 'n:m:', ["number1", "number2"])


def recur_fibo(n):
    if n <= 1:
        return n
    else:
        return(recur_fibo(n-1) + recur_fibo(n-2))


if __name__ == "__main__":
    for (op, ar) in opt:
        if (op in ['-n', 'number1']):
            n = int(ar)
        if (op in ['-m', 'number2']):
            m = int(ar)

    subtract = m-n
    pool = ProcessPoolExecutor(resta)
    print("-n (must be smaller)")
    while(n < m):
        for i in range(n):
            future = pool.submit(recur_fibo, n)
        n += 1
        print(future.result())
