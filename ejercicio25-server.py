#!/usr/bin/python3
import socket
import sys
import time
import getopt
import os

(opt, arg) = getopt.getopt(sys.argv[1:], 'p:f:')
p = ""
f = ""

for (op, ar) in opt:
    if (op == '-p'):
        port = int(ar)
    elif (op == '-f'):
        f = ('/tmp/' + ar)

serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = ""
serv.bind((host, port))
serv.listen(5)

fd = open(f, "w+")

print('listening...\n')
while True:
    conn, addr = serv.accept()
    
    data = conn.recv(1024)
    if not data:
        break
    address = addr[0]
    port = addr[1]
    print('message: ' + data.decode('ascii') + ' - from Address: %s - Port: %d' % (address, port))
    fd.write('\n' + data.decode('ascii'))
    time.sleep(1)

conn.close()
print('client disconnect')
