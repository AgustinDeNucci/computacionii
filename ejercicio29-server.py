#!/usr/bin/python3
import os
import socket
import sys
import threading
import time
import getopt

(opcion, arg) = getopt.getopt(sys.argv[1:], 'p:')

for (op, ar) in opcion:
    if op == '-p':
        p = int(ar)


def th_server(sock):

    while True:

        opcion = sock.recv(1024)
        print('Client %s : %s' % (addr[0], addr[1]))
        print("Option: " + opcion.decode() + '\n')

        if (opcion.decode() == 'ABRIR'):
            texto = '\nFile name: '
            sock.send(texto.encode())
            archivo = sock.recv(1024).decode()
            fd = open(archivo, 'a+')

        elif (opcion.decode() == 'AGREGAR'):
            fd = open(archivo, 'a+')
            texto = '\nquit to leave\n Texto: '
            sock.send(texto.encode())
            while True:
                message = sock.recv(1024)
                if message.decode() == 'quit':
                    break
                print('Message: ' + message.decode())
                fd.write(message.decode() + '\n')
            print('Finished loop')
            fd.close()

        elif (opcion.decode() == 'LEER'):
            fd = open(archivo, 'r')
            print('\nSending file to the client: ' + archivo + '\n')
            contenido = fd.read()
            sock.sendall(contenido.encode())
            fd.close()

        elif (opcion.decode() == 'CERRAR'):
            fd.close()
            break

        else:
            print('\nInvalid option!\n')


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = p

serversocket.bind((host, port))
serversocket.listen(5)

while True:
    clientsocket, addr = serversocket.accept()
    print("\nConnection established with %s : %s\n" % (addr[0], addr[1]))
    th = threading.Thread(target=th_server, args=(clientsocket,))
    th.start()
clientsocket.close()
