import os
import sys
from multiprocessing import Process, Pipe


def childProcess1(conn):
    sys.stdin = open(0)
    print("\nComience a escribir:\n")
    for line in sys.stdin:
        conn.send("%s" % line)
    conn.close()


def childProcess2(conn2):
    while True:
        line = conn2.recv()
        if line:
            print("\nHijo (%d) escribió: %s" % (os.getpid(), line))
        else:
            sys.exit(0)
    conn2.close()


if __name__ == '__main__':
    parent_conn, child_conn = Pipe()
    p1 = Process(target=childProcess1, args=(child_conn,))
    p2 = Process(target=childProcess2, args=(parent_conn,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()

