from multiprocessing import Process, Queue, Lock
from time import sleep
import os


def child(x, l):
    l.acquire()
    print("Proceso %d, PID: %d" % (x, os.getpid()))
    sleep(x)
    q.put(os.getpid())
    l.release()


def viewQueue():
    while q:
        print("%d\t" % q.get(), end="")
        if q.empty():
            break


if __name__ == "__main__":
    q = Queue()
    lock = Lock()

    for num in range(10):
        p = Process(target=child, args=(num, lock))
        p.start()
        p.join()
    sleep(2)

    os.system('clear')
    print("Los elementos de la cola son:\n")
    
    viewQueue()
    
    print("\n")