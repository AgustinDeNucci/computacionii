#!/usr/bin/python3
import socket
import sys
import os
import getopt
import time

(opt, arg) = getopt.getopt(sys.argv[1:], 'a:p:')

for (op, ar) in opt:
    if op == '-a':
        host = str(ar)
    elif op == '-p':
        port = int(ar)

try:
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to creat socket!')
    sys.exit()

client.connect((host, port))
print('Socket connected to host', host, 'in port', port)

while True:

    print("""\n
    \t\t\t *** Menu ***
    - ABRIR
    - AGREGAR
    - LEER
    - CERRAR
    """)

    opcion = input('Option: ').upper()

    client.sendto(opcion.encode(), (host, port))

    if (opcion == 'ABRIR'):
        print(client.recv(1024).decode())
        data = input()
        archivo = '/tmp/' + data
        client.sendto(archivo.encode(), (host, port))

    elif (opcion == 'AGREGAR'):
        print(client.recv(1024).decode())
        while True:
            msg = input()
            client.sendto(msg.encode(), (host, port))
            if msg == 'quit':
                break

    elif (opcion == 'LEER'):
        contenido = client.recv(1024).decode()
        print('\nFile: ' + archivo + '\n')
        print(contenido)
        input('Press Enter...')

    elif (opcion == 'CERRAR'):
        break

    else:
        print('\nInvalid option!\n')
        input('Press Enter...')

client.close()
