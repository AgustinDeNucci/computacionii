import os


def child():
    os._exit(0)


def parent():
    i = 1
    while True:
        newpid = os.fork()
        if newpid == 0:
            child()
        else:
            pids = (newpid, os.getpid())
            print("\nSoy el hijo: %d y mi padre es: %d\n" % pids)
        if i == 3:
            break
        i = i + 1


parent()
