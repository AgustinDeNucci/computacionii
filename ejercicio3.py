import os, time


def hijo():
    for i in range(5):
        print("\nsoy el hijo %d" % os.getpid())
        time.sleep(1)
    os._exit(0)


def padre():
    newpid = os.fork()
    if newpid == 0:
        hijo()
    newpid, estado = os.wait()
    if estado == 0:
        for i in range(2):
            print("\nsoy el padre %d" % os.getpid())
            time.sleep(1)
        print("\n-----------------------------\n")
        print("Mi proceso hijo %d terminó" % newpid)


padre()
