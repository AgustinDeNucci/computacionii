import os, sys, getopt

(opt, arg) = getopt.getopt(sys.argv[1:], 'n:', ["operador"])


def hijo():
    print("Soy el hijo")
    decres = num - 2
    print("El valor", num, "decrementado en 2 es: ", decres, "\n")
    os._exit(0)


def padre():
    newpid = os.fork()

    if newpid == 0:
        hijo()
    print("\nsoy el padre")
    incres = num + 4
    print("El valor", num, "incrementado en 4 es: ", incres, "\n")


for (op, ar) in opt:
    try:
        num = int(ar)
        padre()
    except ValueError:
        print("La variable debe ser un numero entero, vuelva a ingresar.")
        os._exit(0)

# quiero poner una exepcion para getopt.GetoptError, pero no puedo modificar el error de salida

