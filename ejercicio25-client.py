#!/usr/bin/python3
import socket
import getopt
import time
import sys
import os

(opt, arg) = getopt.getopt(sys.argv[1:], 'a:p:')

a = ""
p = ""

for (op, ar) in opt:
    if (op == '-a'):
        host = ar
    elif (op == '-p'):
        port = int(ar)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))

while True:
    msg = input('Send: ').encode('ascii')
    try:
        s.sendto(msg, (host, port))
    except socket.error:
        print('Error Code: ' + str(msg[0]) + 'Message' + msg[1])
        sys.exit()
sys.exit()
