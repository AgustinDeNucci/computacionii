#!/usr/bin/python3
import socket
import os
import threading
import sys


def th_server(sock):
    while True:
        msg = sock.recv(1024)
        print("Message: %s" % msg.decode('ascii'))
        if not msg:
            break

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = ""
port = 1234

serversocket.bind((host, port))
serversocket.listen(5)

while True:
    clientsocket, addr = serversocket.accept()
    print("Got a connection from %s" % str(addr))
    th = threading.Thread(target=th_server, args=(clientsocket,))
    th.start()
clientsocket.close()
