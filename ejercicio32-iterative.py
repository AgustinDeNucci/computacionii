from concurrent.futures import ProcessPoolExecutor
import sys
import getopt

(opt, arg) = getopt.getopt(sys.argv[1:], 'n:m:', ["number1", "number2"])


def iter_fibo(n):
    a = 1
    b = 0
    for i in range(n):
        a, b = b, a + b
    return b


if __name__ == "__main__":
    for (op, ar) in opt:
        if (op in ['-n', 'number1']):
            n = int(ar)
        if (op in ['-m', 'number2']):
            m = int(ar)

    subtract = m-n
    pool = ProcessPoolExecutor(resta)
    print("-n (must be smaller)")
    while(n < m):
        for i in range(n):
            future = pool.submit(iter_fibo, n)
        n += 1
        print(future.result())
