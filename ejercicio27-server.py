#!/usr/bin/python3
import socket
import os
import multiprocessing
import sys


def mp_server(sock):
    while True:
        msg = sock.recv(1024)
        print("Message: %s" % msg.decode())
        if not msg:
            break

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = ""
port = int(sys.argv[1])

serversocket.bind((host, port))
serversocket.listen(5)

while True:

    clientsocket, addr = serversocket.accept()

    print("Got a connection from %s" % str(addr))
    child = multiprocessing.Process(target=mp_server, args=(clientsocket,))
    child.start()
