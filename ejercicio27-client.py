#!/usr/bin/python3
import socket
import sys
import os
import time

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error:
    print('Failed to creat socket!')
    sys.exit()

host = str(sys.argv[1])
port = int(sys.argv[2])

s.connect((host, port))
print('Socket connected to host', host, 'in port', port)

while True:
    msg = input('Send: ').encode()
    if msg.decode() == 'exit':
        break
    else:
        try:
            # Set the whole string
            s.sendto(msg, (host, port))
        except socket.error:
            # Send failed
            print('Error sending!')
            sys.exit()

s.close()
