from multiprocessing import Process, Pipe


def fifoMessage():
    fd = open("saludoFifo.txt", "r")
    message = fd.readline()
    message = str(message)
    fd.close
    return message


def childProcess(conn):
    linea = conn.recv()
    print("Mensaje: %s" % linea)
    conn.close()


if __name__ == '__main__':
    parent_conn, child_conn = Pipe()
    p1 = Process(target=childProcess, args=(child_conn,))
    p1.start()
    parent_conn.send(fifoMessage())
    parent_conn.close()
    p1.join()
