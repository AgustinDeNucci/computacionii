from multiprocessing import Pool, Queue
from time import sleep
import os


def child(x):
    print("Proceso %d, PID: %d" % (x, os.getpid()))
    sleep(x)
    q.put(os.getpid())


def viewQueue():
    while q:
        print("%d\t" % q.get(), end="")
        if q.empty():
            break
    print("\n")        


if __name__ == "__main__":
    q = Queue()
    pool = Pool()
    pool.map(child, range(10))
    sleep(1)
    print("Los elementos de la lista son:\n")    
    viewQueue()
