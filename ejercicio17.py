
import os
import sys
from time import sleep
from multiprocessing import Queue

def child(x, q):
    print("Proceso %d, PID: %d" % (x, os.getpid()))
    sleep(x)
    q.put(os.getpid())
    sys.exit()

def viewQueue():
    while q:
        print("%d\t" % q.get(), end="")
        if q.empty():
            break
    print("\n")

if __name__ == '__main__':
    q = Queue()
    for x in range(10):
        pid = os.fork()
        if not pid:
            child(x, q)
        else:
            os.wait()

    print("Los elementos de la cola son:\n")
    viewQueue()




