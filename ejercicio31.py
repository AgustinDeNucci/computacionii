import sys
import getopt
import threading
import string
import time

(opt, arg) = getopt.getopt(sys.argv[1:], 'f:r:', ["file", "iterations"])


def f(lock, file_name, n, letter):
    lock.acquire()
    file = open(file_name, 'a')
    for i in range(n):
        file.write(letter)
    time.sleep(1)
    file.close()
    lock.release()


if __name__ == "__main__":
    for (op, ar) in opt:
        if (op in ['-f', 'file']):
            file_name = ar
        if (op in ['-r', 'iterations']):
            cant_iteraciones = int(ar)
    lock = threading.Lock()
    letters = string.ascii_uppercase
    try:
        aux = open(file_name, 'r+')
        contenido = aux.read()
        if contenido != '':
            aux.seek(0)
            aux.truncate()
        aux.close()
    except FileNotFoundError as e:
        file = open(file_name, 'a')
        file.close()
    lista = []
    for i in range(15):
        letter = letters[i]
        lista.append(threading.Thread(target=f, args=(lock, file_name, cant_iteraciones, letter)))
        lista[i].start()
    for i in lista:
        i.join()
