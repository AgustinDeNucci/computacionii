import signal, time


def Sig(sigNum, frame):
    print('\nEsta vez me saldré, inténtalo nuevamente\n')
    signal.signal(signal.SIGINT, signal.SIG_DFL)


signal.signal(signal.SIGINT, Sig)

time.sleep(10)
