import os, time, signal


def padre(s, f):
    print("\nPadre enviando señal...")


def hijo():
    while True:
        print("\nHijo %d: Señal recibida" % os.getpid())
        print("A espera de una nueva señal...")
        signal.pause()


signal.signal(signal.SIGUSR1, padre)
pid = os.fork()

if pid == 0:
    hijo()
else:
    print("\nProceso padre %d iniciado" % os.getpid())
    for item in range(5):
        os.kill(pid, signal.SIGUSR1)
        time.sleep(5)
    print("\nPadre matando al hijo...")
    os.kill(pid, signal.SIGTERM)
    print("Padre terminando...")
