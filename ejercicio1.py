import sys
import getopt

(opt, arg) = getopt.getopt(sys.argv[1:], 'o:n:m:')

print("opciones: ", opt)

f_o = False
f_n = False
f_m = False


for (op, ar) in opt:
    if op in '-o':
        operador = ar
        print("Signo -o: ", ar)
        f_o = True
    elif op == '-n':
        num1 = ar
        print("Numero1 -n: ", ar)
        f_n = True
    elif op == '-m':
        num2 = ar
        print("Numero -m: ", ar)
        f_m = True
    else:
        print("Opcion invalida")

num1 = int(num1)
num2 = int(num2)

if f_o and f_n and f_m:

    if operador == '+':
        cuenta = num1+num2
        print(cuenta)

    if operador == '-':
        cuenta = num1-num2
        print(cuenta)

    if operador == '*':
        cuenta = num1*num2
        print(cuenta)

    if operador == '/':
        cuenta = num1/num2
        print(cuenta)
